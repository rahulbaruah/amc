<div class="container">
                    
    <div class="kingster-item-pdlr">
        <h4 style="margin-bottom:0;">About Jettwings Business School</h4>
    </div>
    
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-white" style="padding-top:0;padding-left: 0;margin-left: 20px;">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">About</a></li>
        <li class="breadcrumb-item active" aria-current="page">About JBS</li>
      </ol>
    </nav>
    <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <img src="https://via.placeholder.com/450x360/000000/ffffff"></img>
            
            <div class="left-sub-box">
                <div class="kingster-sidebar-area kingster-item-pdlr" style="padding-top:0">
                                
                                <div id="text-5" class="widget widget_text kingster-widget" style="background-color: #f7f7f7">
                                        <h3 class="kingster-widget-title">Lorem ipsum</h3><span class="clear"></span>
                                        <div class="textwidget">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat. Nunc aliquet bibendum enim facilisis gravida. Nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper.</p>
                                        </div>
                                </div>
                </div>
                
                <div class="kingster-sidebar-area kingster-item-pdlr" style="padding-top:0">
                                
                                <div id="text-5" class="widget widget_text kingster-widget" style="background-color: #f7f7f7">
                                        <h3 class="kingster-widget-title">Lorem ipsum</h3><span class="clear"></span>
                                        <div class="textwidget">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat. Nunc aliquet bibendum enim facilisis gravida. Nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper.</p>
                                        </div>
                                </div>
                </div>
            </div>
            
            <div class="left-sub-box">
                <img src="https://via.placeholder.com/450x530/ec2f45/ffffff"></img>
            </div>
        </div>
        <div class="col-md-8">
            <ul class="right-box-list">
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"/></div><div class="col-md-8 col-7">Ranked amongst the Prominent B – Schools of INDIA by GHRDC – CSR B School Survey 2019</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"></img></div><div class="col-md-8 col-7">Ranked as the Times Best Professional Institute of North East India in Terms of Quality Training & Placements by Times of India – Business Awards 2019</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"></img></div><div class="col-md-8 col-7">Jettwings Business School – (JBS)   is affiliated under the prestigious Government of Assam State University - Gauhati University (NAAC RATING 5 STAR) & Approved by Department of Higher Education, Government of ASSAM</div></div></li>
            </ul>
            
            <div class="right-sub-box">
            <img src="https://via.placeholder.com/900x280/000000/ffffff"></img>
            <p>Amid picturesque locales, rich flora and fauna, composite culture, and sprawling across 1.5 acres of a green campus, a new citadel of education has emerged—Jettwings Business School shortly known as JBS. Placing its campus at Nabagraha Hills in the middle of historical Guwahati city, JBS ensures a blissful learning experience for each of its students. Ranked among Top Prominent B-Schools of India by Higher Education Review 2018 Survey, and Top Ranked Institute by CSR –Educational Institutes Survey 2019, JBS has crafted a course curriculum to help students with a capacity for analysis, assessment, judgment, and action to help business enterprises grow to the next level.&nbsp;&nbsp;<a href="#" id="rdMoreBtn" class="text-danger">Read More <i class="fa fa-angle-right" aria-hidden="true"></i></a></p>
            <p id="more">The world of Jettwings Business School (JBS) revolves around a constellation of values that harmonizes business acumen with a shared sense of responsibility towards environmental safety. We create thought leaders of change who deliver solutions that will drive modern-day business modules. At JBS, we promote a learning methodology focussed on entrepreneurial and growth-driven initiatives at a global level. We inspire next-generation business leaders who can harness technology and derive innovations to foster growth at a regional and global scale.<br/>
            Jettwings Business School (JBS) has been very carefully crafted to help students develop a capacity for analysis, assessment, judgment, and action that they can exercise throughout the course of any career that they choose to pursue. Here, students undergo in-depth, case-method learning that puts them in the role of the decision-maker. In-depth case studies, seminars, Industry integration and interface under unmatched guidance take students to the frontiers of new markets, practices, and technologies across the globe. So, welcome to be a part of a richer learning experience.&nbsp;&nbsp;<a href="#" id="rdLessBtn" class="text-danger"><i class="fa fa-angle-left" aria-hidden="true"></i> Read Less</a></p>
            </div>
            
            <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
                <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #000000; ;border-bottom-width: 1px ;"></div>
            </div>
            
            <div class="right-sub-box">
            <h3>Campus Features</h3>
            <p>Jettwings Business School intends to roll out curricula in higher education that promotes:</p>
            <ul>
                <li>Industry-ready professionals with a conscience for a safe and secure environment.</li>
                <li>Quality education from an eclectic group of faculty with state-of-the-art training equipment including proper student-monitoring.</li>
                <li>Affluent-enough personality development to match the international level of business & entrepreneurship.</li>
            </ul>
            <a class="btn bg-light btn-secondary btn-lg" href="#">View all campus features</a>
            </div>
            
            <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
                <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #000000; ;border-bottom-width: 1px ;"></div>
            </div>
            
            <div class="right-sub-box">
            <h3>Our Core features</h3>
            <p>The Core Focus Area of JBS is Top of the Line Placements The career development process includes self-awareness, career exploration, and job placement.</p>
            <p><b>JBS’s prime focus areas are:</b>
            
            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-icon-list-item gdlr-core-item-pdlr gdlr-core-item-pdb clearfix " style="padding-bottom: 25px ;">
                                                    <ul>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Teaching Methodology
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Collaborative Programs
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                JBS International Program Orientation
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Strong Training & Placement Strategy
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Industry Networking
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Campus Placement
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Overseas Placement
                                                            </span></div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
            
            </p>
            <a class="btn bg-light btn-secondary btn-lg" href="#">View all campus features</a>
            </div>
        </div>
    </div>

</div>

<div class="gdlr-core-pbf-wrapper " style="padding: 65px 0px 60px 0px;margin-top:50px">
                        <div class="gdlr-core-pbf-background-wrap" style="background-color: #eeeeee ;"></div>
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 45px 0px 0px 0px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 20px ;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 23px ;text-transform: none ;color: #000000 ;">
                                                        <p>Through JBS we would like to promote a new-aged education that will result in a special breed of professionals from a multi-cultural ethnicity and prepare them for diversified global work culture.
<br/>
<b>So, come be a part of a richer learning experience</b>
</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align"><a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="#" style="font-size: 14px ;font-weight: 700 ;letter-spacing: 0px ;padding: 13px 26px 16px 30px;text-transform: none ;margin: 0px 10px 10px 0px;border-radius: 2px;-moz-border-radius: 2px;-webkit-border-radius: 2px;background: #ec2f45 ;"><span class="gdlr-core-content" >Apply Now</span></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30" id="gdlr-core-column-1">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="margin: -123px 0px 0px 0px;padding: 0px 0px 0px 40px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align">
                                                    <div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;"><img src="http://via.placeholder.com/400x257/000000/ffffff" width="700" height="450"  alt="" /></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
<script type="text/javascript">
    document.getElementById("rdMoreBtn").addEventListener("click", function(event){
        event.preventDefault();
        readMoreFunction();
    });
    
    document.getElementById("rdLessBtn").addEventListener("click", function(event){
        event.preventDefault();
        readMoreFunction();
    });
    
    function readMoreFunction() {
        var moreText = document.getElementById("more");
        var rdMoreBtn = document.getElementById("rdMoreBtn");
        if (moreText.style.display === "inline") {
            rdMoreBtn.style.display = "inline";
            moreText.style.display = "none";
          } else {
            rdMoreBtn.style.display = "none";
            moreText.style.display = "inline";
          }
    }
</script>