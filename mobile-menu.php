<div class="kingster-mobile-menu"><a class="kingster-mm-menu-button kingster-mobile-menu-button kingster-mobile-button-hamburger" href="#kingster-mobile-menu"><span></span></a>
                        <div class="kingster-mm-menu-wrap kingster-navigation-font" id="kingster-mobile-menu" data-slide="right">
                            <ul id="menu-main-navigation" class="m-menu">
                                <li class="menu-item"><a href="/">Home</a></li>
                                <li class="menu-item"><a href="#">About Us</a></li>
                                <li class="menu-item menu-item-home menu-item-has-children"><a href="#">Events</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-home"><a href="#">Previous</a></li>
                                        <li class="menu-item"><a href="#">Upcoming</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a href="#">Departments</a></li>
                                <li class="menu-item"><a href="#">Tenders</a></li>
                                
                                <li class="menu-item menu-item-home menu-item-has-children"><a href="#">Notices</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-home"><a href="#">Students Notice</a></li>
                                        <li class="menu-item"><a href="#">Inhouse Notice</a></li>
                                        <li class="menu-item"><a href="#">General Notice</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a href="#">Careers</a></li>
                                <li class="menu-item"><a href="#">Library</a></li>
                                <li class="menu-item"><a href="#">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>