<div class="kingster-page-wrapper" id="kingster-page-wrapper" style="padding-top:30px">
                <div class="container">
                    
    <div class="kingster-item-pdlr">
        <h4 style="margin-bottom:0;">Courses</h4>
    </div>
    
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-white" style="padding-top:0;padding-left: 0;margin-left: 20px;">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Academics</a></li>
        <li class="breadcrumb-item active" aria-current="page">Courses</li>
      </ol>
    </nav>
    <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
                    
                    <div class=" kingster-sidebar-wrap clearfix kingster-line-height-0 kingster-sidebar-style-right">
                        <div class=" kingster-sidebar-center kingster-column-40 kingster-line-height">
                            <div class="gdlr-core-page-builder-body">
                                <div class="gdlr-core-pbf-wrapper" style="padding-top:0">
                                    <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                                        <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 40px ;">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 20px ;font-weight: 600 ;">BBA DEGREE / DIPLOMA PROGRAMS</h3></div>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-course-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-course-style-list" style="padding-bottom: 45px ;">
                                                    <div class="gdlr-core-course-item-list"><a class="gdlr-core-course-item-link" href="#"><span class="gdlr-core-course-item-id gdlr-core-skin-caption" >JW123</span><span class="gdlr-core-course-item-title gdlr-core-skin-title" >BBA in Aviation Management</span><i class="gdlr-core-course-item-icon gdlr-core-skin-icon fa fa-chevron-right " ></i></a></div>
                                                    <div class="gdlr-core-course-item-list"><a class="gdlr-core-course-item-link" href="#"><span class="gdlr-core-course-item-id gdlr-core-skin-caption" >JW123</span><span class="gdlr-core-course-item-title gdlr-core-skin-title" >BBA in Aviation Management</span><i class="gdlr-core-course-item-icon gdlr-core-skin-icon fa fa-chevron-right " ></i></a></div>
                                                    <div class="gdlr-core-course-item-list"><a class="gdlr-core-course-item-link" href="#"><span class="gdlr-core-course-item-id gdlr-core-skin-caption" >JW123</span><span class="gdlr-core-course-item-title gdlr-core-skin-title" >BBA in Aviation Management</span><i class="gdlr-core-course-item-icon gdlr-core-skin-icon fa fa-chevron-right " ></i></a></div>
                                                    <div class="gdlr-core-course-item-list"><a class="gdlr-core-course-item-link" href="#"><span class="gdlr-core-course-item-id gdlr-core-skin-caption" >JW123</span><span class="gdlr-core-course-item-title gdlr-core-skin-title" >BBA in Aviation Management</span><i class="gdlr-core-course-item-icon gdlr-core-skin-icon fa fa-chevron-right " ></i></a></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 40px ;">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 20px ;font-weight: 600 ;">POST GRADUATE DEGREE / DIPLOMA PROGRAMS</h3></div>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-course-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-course-style-list">
                                                    <div class="gdlr-core-course-item-list"><a class="gdlr-core-course-item-link" href="#"><span class="gdlr-core-course-item-id gdlr-core-skin-caption" >JW123</span><span class="gdlr-core-course-item-title gdlr-core-skin-title" >BBA in Aviation Management</span><i class="gdlr-core-course-item-icon gdlr-core-skin-icon fa fa-chevron-right " ></i></a></div>
                                                    <div class="gdlr-core-course-item-list"><a class="gdlr-core-course-item-link" href="#"><span class="gdlr-core-course-item-id gdlr-core-skin-caption" >JW123</span><span class="gdlr-core-course-item-title gdlr-core-skin-title" >BBA in Aviation Management</span><i class="gdlr-core-course-item-icon gdlr-core-skin-icon fa fa-chevron-right " ></i></a></div>
                                                    <div class="gdlr-core-course-item-list"><a class="gdlr-core-course-item-link" href="#"><span class="gdlr-core-course-item-id gdlr-core-skin-caption" >JW123</span><span class="gdlr-core-course-item-title gdlr-core-skin-title" >BBA in Aviation Management</span><i class="gdlr-core-course-item-icon gdlr-core-skin-icon fa fa-chevron-right " ></i></a></div>
                                                    <div class="gdlr-core-course-item-list"><a class="gdlr-core-course-item-link" href="#"><span class="gdlr-core-course-item-id gdlr-core-skin-caption" >JW123</span><span class="gdlr-core-course-item-title gdlr-core-skin-title" >BBA in Aviation Management</span><i class="gdlr-core-course-item-icon gdlr-core-skin-icon fa fa-chevron-right " ></i></a></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 40px ;">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 20px ;font-weight: 600 ;">TATA INSTITUTE OF SOCIAL SCIENCES PROGRAMS</h3></div>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-course-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-course-style-list">
                                                    <div class="gdlr-core-course-item-list"><a class="gdlr-core-course-item-link" href="#"><span class="gdlr-core-course-item-id gdlr-core-skin-caption" >JW123</span><span class="gdlr-core-course-item-title gdlr-core-skin-title" >BBA in Aviation Management</span><i class="gdlr-core-course-item-icon gdlr-core-skin-icon fa fa-chevron-right " ></i></a></div>
                                                    <div class="gdlr-core-course-item-list"><a class="gdlr-core-course-item-link" href="#"><span class="gdlr-core-course-item-id gdlr-core-skin-caption" >JW123</span><span class="gdlr-core-course-item-title gdlr-core-skin-title" >BBA in Aviation Management</span><i class="gdlr-core-course-item-icon gdlr-core-skin-icon fa fa-chevron-right " ></i></a></div>
                                                    <div class="gdlr-core-course-item-list"><a class="gdlr-core-course-item-link" href="#"><span class="gdlr-core-course-item-id gdlr-core-skin-caption" >JW123</span><span class="gdlr-core-course-item-title gdlr-core-skin-title" >BBA in Aviation Management</span><i class="gdlr-core-course-item-icon gdlr-core-skin-icon fa fa-chevron-right " ></i></a></div>
                                                    <div class="gdlr-core-course-item-list"><a class="gdlr-core-course-item-link" href="#"><span class="gdlr-core-course-item-id gdlr-core-skin-caption" >JW123</span><span class="gdlr-core-course-item-title gdlr-core-skin-title" >BBA in Aviation Management</span><i class="gdlr-core-course-item-icon gdlr-core-skin-icon fa fa-chevron-right " ></i></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" kingster-sidebar-right kingster-column-20 kingster-line-height kingster-line-height">
                            <div class="kingster-sidebar-area kingster-item-pdlr" style="padding-top:0">
                                <div id="text-18" class="widget widget_text kingster-widget">
                                    <div class="textwidget">
                                        <div class="gdlr-core-widget-box-shortcode " style="color: #ffffff ;padding: 30px 45px;background-color: #ec2f45 ;">
                                            <div class="gdlr-core-widget-box-shortcode-content">
                                                </p>
                                                <h3 style="font-size: 20px; color: #fff; margin-bottom: 25px;">Contact Info</h3>
                                                <p><span style="font-size: 15px;">Nabagraha Road, Chenikuthi
                                                    <br /> Hill Side, Silpukhuri<br /> Gauhati - 781003</span></p>
                                                <p><span style="font-size: 15px;"><i class="fa fa-phone" aria-hidden="true"></i> (+91) 99540-90900<br /><i class="fa fa-envelope-o" aria-hidden="true"></i> info@jettwingsbschool.com<br /> </span></p>
                                                <p><span style="font-size: 16px;"><i class="fa fa-clock-o" aria-hidden="true"></i> Mon &#8211; Sat 9:00A.M. &#8211; 5:00P.M.</span></p> <span class="gdlr-core-space-shortcode" style="margin-top: 40px ;"></span>
                                                <span class="gdlr-core-space-shortcode" style="margin-top: 40px ;"></span> <a class="gdlr-core-button gdlr-core-button-shortcode  gdlr-core-button-gradient gdlr-core-button-no-border" href="#" style="padding: 16px 27px 18px;margin-right: 20px;border-radius: 2px;-webkit-border-radius: 2px;"><span class="gdlr-core-content" style="color:#111">Apply Now</span></a>
                                                <p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>