<div class="container">
                    
    <div class="kingster-item-pdlr">
        <h4 style="margin-bottom:0;">Awards and Associations</h4>
    </div>
    
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-white" style="padding-top:0;padding-left: 0;margin-left: 20px;">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">About</a></li>
        <li class="breadcrumb-item active" aria-current="page">Awards and Associations</li>
      </ol>
    </nav>
    <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <ul class="right-box-list">
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"/></div><div class="col-md-8 col-7">Ranked amongst the Prominent B – Schools of INDIA by GHRDC – CSR B School Survey 2019</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"></img></div><div class="col-md-8 col-7">Ranked as the Times Best Professional Institute of North East India in Terms of Quality Training & Placements by Times of India – Business Awards 2019</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"></img></div><div class="col-md-8 col-7">Jettwings Business School – (JBS)   is affiliated under the prestigious Government of Assam State University - Gauhati University (NAAC RATING 5 STAR) & Approved by Department of Higher Education, Government of ASSAM</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"/></div><div class="col-md-8 col-7">Awarded in category of -  India’s Best Institute - Placements “ in INDIA SKILL SUMMIT in New Delhi , by ASSOCHAM in association with Ministry of Labour & Employment Government Of India & Ministry of Skill Development Government of India</div></div></li>
            </ul>
        </div>
        <div class="col-md-6">
            <ul class="right-box-list">
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"></img></div><div class="col-md-8 col-7">Ranked amongst the top Institutes by CSR – Educational Institutes Survey 2019</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"></img></div><div class="col-md-8 col-7">Ranked among the Top Ten best Institutes in Travel & Tourism by Higher Education review 2019 Survey.</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"></img></div><div class="col-md-8 col-7">Ranked amongst the Top Ten Aviation Institutes by Higher Education Review 2018 Survey</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"></img></div><div class="col-md-8 col-7">Most Trusted Educational Institute of the Year, 2019 by Business Sight International Magazine.</div></div></li>
            </ul>
        </div>
    </div>
    
    <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
                <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #000000; ;border-bottom-width: 1px ;"></div>
            </div>
    
            <div class="right-sub-box">
            <h3>Our Associations</h3>
            
            <div class="row">
        <div class="col-md-6">
            <ul class="right-box-list">
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff"/></div><div class="col-md-8 col-7">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat.</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff"></img></div><div class="col-md-8 col-7">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat.</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff"></img></div><div class="col-md-8 col-7">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat.</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff"/></div><div class="col-md-8 col-7">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat.</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff"/></div><div class="col-md-8 col-7">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat.</div></div></li>
            </ul>
        </div>
        <div class="col-md-6">
            <ul class="right-box-list">
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff"/></div><div class="col-md-8 col-7">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat.</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff"></img></div><div class="col-md-8 col-7">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat.</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff"></img></div><div class="col-md-8 col-7">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat.</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff"/></div><div class="col-md-8 col-7">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat.</div></div></li>
            </ul>
        </div>
    </div>
            
            </div>
            
            
</div>

<div class="gdlr-core-pbf-wrapper " style="padding: 65px 0px 60px 0px;margin-top:50px">
                        <div class="gdlr-core-pbf-background-wrap" style="background-color: #eeeeee ;"></div>
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 45px 0px 0px 0px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 20px ;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 23px ;text-transform: none ;color: #000000 ;">
                                                        <p>Through JBS we would like to promote a new-aged education that will result in a special breed of professionals from a multi-cultural ethnicity and prepare them for diversified global work culture.
<br/>
<b>So, come be a part of a richer learning experience</b>
</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align"><a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="#" style="font-size: 14px ;font-weight: 700 ;letter-spacing: 0px ;padding: 13px 26px 16px 30px;text-transform: none ;margin: 0px 10px 10px 0px;border-radius: 2px;-moz-border-radius: 2px;-webkit-border-radius: 2px;background: #ec2f45 ;"><span class="gdlr-core-content" >Apply Now</span></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30" id="gdlr-core-column-1">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="margin: -123px 0px 0px 0px;padding: 0px 0px 0px 40px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align">
                                                    <div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;"><img src="http://via.placeholder.com/400x257/000000/ffffff" width="700" height="450"  alt="" /></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
<script type="text/javascript">
    document.getElementById("rdMoreBtn").addEventListener("click", function(event){
        event.preventDefault();
        readMoreFunction();
    });
    
    document.getElementById("rdLessBtn").addEventListener("click", function(event){
        event.preventDefault();
        readMoreFunction();
    });
    
    function readMoreFunction() {
        var moreText = document.getElementById("more");
        var rdMoreBtn = document.getElementById("rdMoreBtn");
        if (moreText.style.display === "inline") {
            rdMoreBtn.style.display = "inline";
            moreText.style.display = "none";
          } else {
            rdMoreBtn.style.display = "none";
            moreText.style.display = "inline";
          }
    }
</script>