<div class="container">
                    
    <div class="kingster-item-pdlr">
        <h4 style="margin-bottom:0;">Message from the Chairman</h4>
    </div>
    
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-white" style="padding-top:0;padding-left: 0;margin-left: 20px;">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">About</a></li>
        <li class="breadcrumb-item active" aria-current="page">Chairman's Message</li>
      </ol>
    </nav>
    <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
    <div class="row">
        <div class="col-md-4">
            
            <div class="left-sub-box">
                
                <div class="kingster-sidebar-area kingster-item-pdlr" style="padding-top:0">
                                
                                <div id="text-5" class="widget widget_text kingster-widget" style="background-color: #f7f7f7">
                                        <h3 class="kingster-widget-title">Lorem ipsum</h3><span class="clear"></span>
                                        <div class="textwidget">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat. Nunc aliquet bibendum enim facilisis gravida. Nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper.</p>
                                        </div>
                                </div>
                </div>
                
            </div>
            
        </div>
        <div class="col-md-8">
            <div class="col-md-7">
            <div class="right-sub-box">
            <h4 class="text-danger">Mr. Sanjay Aditya Singha</h4>
            <p>Chairman and Founder<br/>Jettwings Group of Institutes</p>
            </div>
            
            <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
                <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #000000; ;border-bottom-width: 1px ;"></div>
            </div>
            </div>
            <div class="right-sub-box">
                <img src="https://via.placeholder.com/350x450/eeeeee/111111" align="right">
            <p>
                As part of our core mission of creating community prosperity through future market leaders, we pledge to bring forward our youths in a world of diversified ethnicity at a cross-cultural confluence which will make them truly global citizens. We train our youths with a greater sense of responsibility to transform them from learners into leaders.
            </p>
            <p>
                We inspire our students to indulge in multi-cultural engagements to be tolerant and respectful towards all and make a place in the community as a true leader.
            </p>
            <p>
                We seek to create a highly influential industry leaders through our carefully crafted curricula amalgamated with industry-fed insights.
            </p>
            <p>
                As we are on the crest of this technology wave all around us; it becomes more imperative for us to co-create a work force that has a socio-environmental conscience.
            </p>
            <p>
                We are on a mission to deliver new-age leaders to the industry who can accept, adopt, and adhere to the changing business norms, and can find more agreeable technology scale up for their enterprises.
            </p>
            <p>
                Come and experience a learning that harnesses your skills with a shared sense of socio-environmental integration within a zero-carbon-foot-print campus and brings the best out of you with a hi-tech collaborative learning.
            </p>
            
            </div>
            
        </div>
    </div>

</div>