<div class="container">
                    
    <div class="kingster-item-pdlr">
        <h4 style="margin-bottom:0;">Jettwings Story</h4>
    </div>
    
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-white" style="padding-top:0;padding-left: 0;margin-left: 20px;">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">About</a></li>
        <li class="breadcrumb-item active" aria-current="page">Our Story</li>
      </ol>
    </nav>
    <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <img src="https://via.placeholder.com/450x360/000000/ffffff"></img>
            
            <div class="left-sub-box">
                
                <div class="kingster-sidebar-area kingster-item-pdlr" style="padding-top:0">
                                
                                <div id="text-5" class="widget widget_text kingster-widget" style="background-color: #f7f7f7">
                                        <h3 class="kingster-widget-title">Lorem ipsum</h3><span class="clear"></span>
                                        <div class="textwidget">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat. Nunc aliquet bibendum enim facilisis gravida. Nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper.</p>
                                        </div>
                                </div>
                </div>
                
                <div class="kingster-sidebar-area kingster-item-pdlr" style="padding-top:0">
                                
                                <div id="text-5" class="widget widget_text kingster-widget" style="background-color: #f7f7f7">
                                        <h3 class="kingster-widget-title">Lorem ipsum</h3><span class="clear"></span>
                                        <div class="textwidget">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat. Nunc aliquet bibendum enim facilisis gravida. Nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper.</p>
                                        </div>
                                </div>
                </div>
                
            </div>
            
            <div class="left-sub-box">
                <img src="https://via.placeholder.com/450x530/ec2f45/ffffff"></img>
            </div>
        </div>
        <div class="col-md-8">
            <ul class="right-box-list">
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"/></div><div class="col-md-8 col-7">Awarded in category of -  India’s Best Institute - Placements “ in INDIA SKILL SUMMIT in New Delhi , by ASSOCHAM in association with Ministry of Labour & Employment Government Of India & Ministry of Skill Development Government of India</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"></img></div><div class="col-md-8 col-7">Ranked amongst the top Institutes by CSR – Educational Institutes Survey 2019</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"></img></div><div class="col-md-8 col-7">Ranked amongst the Top Ten Aviation Institutes by Higher Education Review 2018 Survey</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"></img></div><div class="col-md-8 col-7">Ranked among the Top Ten best Institutes in Travel & Tourism by Higher Education review 2019 Survey</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"></img></div><div class="col-md-8 col-7">Most Trusted Educational Institute of the Year, 2019 by Business Sight International Magazine</div></div></li>
                <li><div class="row"><div class="col-md-2 col-3"><img src="https://via.placeholder.com/100/000000/ffffff" class="rounded-circle"></img></div><div class="col-md-8 col-7">Times Best Professional Institute by Times of India business Awards 2019</div></div></li>
            </ul>
            <a class="btn btn-primary bg-light" href="#">View More</a>
            
            <div class="right-sub-box">
            <img src="https://via.placeholder.com/900x280/000000/ffffff"></img>
            <p>Characterized by its picturesque locales, rich flora and fauna, composite culture, a huge pool of natural resources, a reservoir of bourgeoning human capital and the sanguine temperament of the local folk, the Northeast is the unexplored yet awe-inspiring region of India. Despite these vantage points and the potential to generate abundant wealth for the country, NER has been underrated, overlooked and its resources under-exploited.
            <br/>
            In the year 2006 , Infovalley Educational & Research (P) Ltd Established the Brand Jettwings & Its First Institute in the name of “Jettwings”, a Premier  Institute of Aviation  Hospitality  & Tourism Management”, at Guwahati ASSAM .
            <br/>
 Jettwings became the first ever private, non-aided professional training provider in Aviation & Hospitality Sector that originated from the Northeast India and quickly expanded its footprint across India & South East Asia by setting up its campuses across major cities. It was one such venture initiated to provide an enduring and crucial platform to the youth of Northeast INDIA in the field of Quality Skill Education which the region is best at and that is AVIATION & HOSPITALITY Sector.
            &nbsp;&nbsp;
            <a href="#" id="rdMoreBtn" class="text-danger">Read More <i class="fa fa-angle-right" aria-hidden="true"></i></a></p>
            <p id="more">In order to truly comprehend the splendor of the region, one must leave all preconceived notions behind and come here with a receptive mind. North-East India has seen significant development in the industrial and service sector but that growth has not been rapid enough. Bearing this in mind and with a quest to bring northeast to the forefront 
<br/>
It was the time when the Indian service sector was flourishing and the other sectors abating, Jettwings was a step in the right direction. As of today, India’s services share of GVA grew at a CAGR of 13.2 percent between 2014-15 and 2018-19 and is expected to grow at 8.3 percent in the coming year. These numbers promise and assure a secure opportunity to the youth and simultaneously transform human capital into human resources in AVIATION & SERVICES SECTOR.
&nbsp;&nbsp;
<a href="#" id="rdLessBtn" class="text-danger"><i class="fa fa-angle-left" aria-hidden="true"></i> Read Less</a>
            </p>
            </div>
            
            <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
                <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #000000; ;border-bottom-width: 1px ;"></div>
            </div>
            
            <div class="right-sub-box">
            <h3>Jettwings Highlight</h3>
            <p>Jettwings have persistently come a long way in terms of dedicated training and placements and have reached the position of a brand most sought after by recruiters and students alike that only a few in the national and global arena can match up to. </p>
            
            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-accordion-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-accordion-style-background-title-icon gdlr-core-left-align gdlr-core-icon-pos-right">
                                                    <div class="gdlr-core-accordion-item-tab clearfix  gdlr-core-active">
                                                        <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon "></div>
                                                        <div class="gdlr-core-accordion-item-content-wrapper">
                                                            <h4 class="gdlr-core-accordion-item-title gdlr-core-js  gdlr-core-skin-e-background gdlr-core-skin-e-content">97%+ Average Placement</h4>
                                                            <div class="gdlr-core-accordion-item-content">
                                                                <p>Since its inception, Jettwings – North East India Training Centres alone with 97+ % average placement has trained and placed more than 14100 + Students working in India and 14 other nations.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="gdlr-core-accordion-item-tab clearfix ">
                                                        <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon "></div>
                                                        <div class="gdlr-core-accordion-item-content-wrapper">
                                                            <h4 class="gdlr-core-accordion-item-title gdlr-core-js  gdlr-core-skin-e-background gdlr-core-skin-e-content">Jettwings’ stature in 5 star Hospitality and airlines</h4>
                                                            <div class="gdlr-core-accordion-item-content">
                                                                <p>North East India represents a mere 3.75% of the country’s entire population, but JETTWINGS’ stature in 5 star hospitality and Airlines has helped NE India to raise that figure to 29% of the total  employees in India Working in AVIATION & HOSPITALITY SECTOR.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="gdlr-core-accordion-item-tab clearfix ">
                                                        <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon "></div>
                                                        <div class="gdlr-core-accordion-item-content-wrapper">
                                                            <h4 class="gdlr-core-accordion-item-title gdlr-core-js  gdlr-core-skin-e-background gdlr-core-skin-e-content">140+ Global Recruiters associated</h4>
                                                            <div class="gdlr-core-accordion-item-content">
                                                                <p>Jettwings is working towards scaling new heights and pushing North east in league of Skilled Human Resources Reserve. Apart from Top Hospitality companies like – The Oberoi, The Westin, The Le Meridian, The ITC Group, The Taj Group, J W Marriott, The Landmark-Dubai , Leela Kapinski ,Hilton , Raj Vilas, Hampshire Resorts regular recruiters list also include Big Aviation Brands Such as – Air Asia, Indigo, Go Air, Spice Jet, Vistara. To top it all, Jettwings manages to get these companies on campus for the placement of its trainees. There are more than 140 domestic and international Recruiters associated with Jettwings.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
            
            </div>
            
            <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
                <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #000000; ;border-bottom-width: 1px ;"></div>
            </div>
            
            <div class="right-sub-box">
            <h3>Jettwings Foundation</h3>
            <p>Education is emerging as the most powerful platform to progress in this “Boundary-less world”. </p>
            <p>Jettwings foundation reaffirms its commitment in its charter to enhance the advancement of quality education and knowledge. Jettwings foundation is established to impart quality training and consultancy to Promote Teaching, Training, and Research in Management Science, Computer science, Biological Science, Engineering, Medical Science and Allied discipline by providing the proper guidance and training. We ensure that our students get the best opportunity to widen their career prospects. We cater to the needs of all the students in the best possible way whoever needs to pursue a higher academic career.</p>
            </div>
            
            <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
                <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #000000; ;border-bottom-width: 1px ;"></div>
            </div>
            
            <div class="right-sub-box">
            <h3>Jettwings Group of Institutes </h3>
            <p>JETTWINGS (JW) - A Premier Institute of Aviation, Hospitality & Tourism Management , Jettwings B School (JBS), Jettwings Aviation College ( Jettwings Aviation Centre of Excellence - JACe) & Jettwings Institute of Fashion Design & Architecture  (JFDA) are leading Institutes under congregate of  “Jettwings Group Institutes”, Managed by Jettwings Foundation & Promoted by Infovalley Educational & Research (P) Ltd. </p>
            <p>JETTWINGS GROUP Offers Industry Oriented Job Ready Professional Courses for Aviation, Hospitality, Tourism, Business Management, Logistics, Fashion, Design & Architecture.</p>
            <p>Under AICTE – NSQF, NSDC, SSC, TISS-SVE & Gauhati University. Through its Top Rated Institutes. The Institutions Under the Group are:</p>
            
            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-accordion-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-accordion-style-background-title-icon gdlr-core-left-align gdlr-core-icon-pos-right">
                                                    <div class="gdlr-core-accordion-item-tab clearfix  gdlr-core-active">
                                                        <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon "></div>
                                                        <div class="gdlr-core-accordion-item-content-wrapper">
                                                            <h4 class="gdlr-core-accordion-item-title gdlr-core-js  gdlr-core-skin-e-background gdlr-core-skin-e-content">Jettwings Institute</h4>
                                                            <div class="gdlr-core-accordion-item-content">
                                                                <p>A Premier Institute of Aviation, Hospitality & Tourism Management (JWI)</p>
                                                                <ol>
                                                                    <li>Aviation Management</li>
                                                                    <li>Hospitality & Hotel Management</li>
                                                                    <li>Travel & Tourism Management</li>
                                                                    <li>Logistics & Retail Management</li>
                                                                </ol>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="gdlr-core-accordion-item-tab clearfix ">
                                                        <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon "></div>
                                                        <div class="gdlr-core-accordion-item-content-wrapper">
                                                            <h4 class="gdlr-core-accordion-item-title gdlr-core-js  gdlr-core-skin-e-background gdlr-core-skin-e-content">Jettwings Aviation College, (JACe Guwahati)</h4>
                                                            <div class="gdlr-core-accordion-item-content">
                                                                <p>The Only COE in Aviation in NER</p>
                                                                <ol>
                                                                    <li>Pilot Training</li>
                                                                    <li>Cabin Crew Training</li>
                                                                    <li>Airport Operations & Security</li>
                                                                    <li>Avionics & MRO</li>
                                                                </ol>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="gdlr-core-accordion-item-tab clearfix ">
                                                        <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon "></div>
                                                        <div class="gdlr-core-accordion-item-content-wrapper">
                                                            <h4 class="gdlr-core-accordion-item-title gdlr-core-js  gdlr-core-skin-e-background gdlr-core-skin-e-content">Jettwings Business School (JBS)</h4>
                                                            <div class="gdlr-core-accordion-item-content">
                                                                <ol>
                                                                    <li>School of Aviation Management</li>
                                                                    <li>School of Hospitality & Tourism</li>
                                                                    <li>School of Retail & Logistics Management</li>
                                                                    <li>School of Management & Commerce</li>
                                                                </ol>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="gdlr-core-accordion-item-tab clearfix ">
                                                        <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon "></div>
                                                        <div class="gdlr-core-accordion-item-content-wrapper">
                                                            <h4 class="gdlr-core-accordion-item-title gdlr-core-js  gdlr-core-skin-e-background gdlr-core-skin-e-content">The Hotel & Travel School – a concept by Jettwings</h4>
                                                            <div class="gdlr-core-accordion-item-content">
                                                                <ol>
                                                                    <li>School of Hotel Management</li>
                                                                    <li>School Tourism Studies</li>
                                                                </ol>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="gdlr-core-accordion-item-tab clearfix ">
                                                        <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon "></div>
                                                        <div class="gdlr-core-accordion-item-content-wrapper">
                                                            <h4 class="gdlr-core-accordion-item-title gdlr-core-js  gdlr-core-skin-e-background gdlr-core-skin-e-content">Jettwings Institute of Fashion, Design & Architecture (JiFDA)</h4>
                                                            <div class="gdlr-core-accordion-item-content">
                                                                <ol>
                                                                    <li>School of Fashion  & Textile Technology</li>
                                                                    <li>School of Interior Design</li>
                                                                    <li>School of Architecture</li>
                                                                    <li>School of Design Communications & Advertising</li>
                                                                </ol>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
            
            </div>
            
        </div>
    </div>

</div>
                    
<script type="text/javascript">
    document.getElementById("rdMoreBtn").addEventListener("click", function(event){
        event.preventDefault();
        readMoreFunction();
    });
    
    document.getElementById("rdLessBtn").addEventListener("click", function(event){
        event.preventDefault();
        readMoreFunction();
    });
    
    function readMoreFunction() {
        var moreText = document.getElementById("more");
        var rdMoreBtn = document.getElementById("rdMoreBtn");
        if (moreText.style.display === "inline") {
            rdMoreBtn.style.display = "inline";
            moreText.style.display = "none";
          } else {
            rdMoreBtn.style.display = "none";
            moreText.style.display = "inline";
          }
    }
</script>