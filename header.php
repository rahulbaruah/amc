<header class="kingster-header-wrap kingster-header-style-plain  kingster-style-menu-right kingster-sticky-navigation kingster-style-fixed" data-navigation-offset="75px">
                <div class="kingster-header-background"></div>
                <div class="kingster-header-container  kingster-container">
                    <div class="kingster-header-container-inner clearfix">
                        <div class="kingster-logo  kingster-item-pdlr">
                            <div class="kingster-logo-inner">
                                <a class="" href="/"><img src="images/Assam-Medical-College-logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="kingster-navigation kingster-item-pdlr clearfix ">
                            <div class="kingster-main-menu" id="kingster-main-menu">
                                <ul id="menu-main-navigation-1" class="sf-menu">
                                    <li class="menu-item kingster-normal-menu"><a href="/">Home</a></li>
                                    
                                    <li class="menu-item kingster-normal-menu"><a href="#">About Us</a></li>
                                    
                                    <li class="menu-item menu-item-home menu-item-has-children kingster-normal-menu"><a href="#">Events <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                        <ul class="sub-menu">
                                            <li class="menu-item" data-size="60"><a href="#">Previous</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Upcoming</a></li>
                                        </ul>
                                    </li>
                                    
                                    <li class="menu-item kingster-normal-menu"><a href="/departments.php">Departments</a></li>
                                    
                                    <li class="menu-item kingster-normal-menu"><a href="#">Tenders</a></li>
                                    
                                    <li class="menu-item menu-item-home menu-item-has-children kingster-normal-menu"><a href="#">Notices <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                        <ul class="sub-menu">
                                            <li class="menu-item" data-size="60"><a href="#">Students Notice</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Inhouse Notice</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">General Notice</a></li>
                                        </ul>
                                    </li>
                                    
                                    <li class="menu-item kingster-normal-menu"><a href="#">Careers</a></li>
                                    <li class="menu-item kingster-normal-menu"><a href="#">Library</a></li>
                                    <li class="menu-item kingster-normal-menu"><a href="#">Contact Us</a></li>
                                </ul>
                                <div class="kingster-navigation-slide-bar" id="kingster-navigation-slide-bar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>